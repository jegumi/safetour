package com.jegumi.safetour.ui;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.jegumi.safetour.R;

public class FlagsActivity extends RoboActivity {

    @InjectView(R.id.gridview)
    private GridView gridView;

    private TypedArray imageIDs;
    private TypedArray nameIDs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flags);

        imageIDs = getResources().obtainTypedArray(R.array.flags_images);
        nameIDs = getResources().obtainTypedArray(R.array.flags_names);
        BadgeAdapter adapter = new BadgeAdapter(this);
        gridView.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        imageIDs.recycle();
        nameIDs.recycle();
        super.onDestroy();
    }

    public class BadgeAdapter extends BaseAdapter {
        private Context context;
        private LayoutInflater layoutInflater;

        BadgeAdapter(Context c) {
            context = c;
            layoutInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return imageIDs.length();
        }

        public Object getItem(int position) {
            return imageIDs.getResourceId(position, -1);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View grid;
            if (convertView == null) {
                grid = new View(context);
                grid = layoutInflater.inflate(R.layout.grid_layout, null);
            } else {
                grid = (View) convertView;
            }

            ImageView imageView = (ImageView) grid.findViewById(R.id.image);
            TextView textView = (TextView) grid.findViewById(R.id.text);

            loadBadgeData(position, grid, imageView, textView);

            return grid;
        }
    }

    private void loadBadgeData(int position, View grid, ImageView imageView, TextView textView) {

        imageView.setImageResource(imageIDs.getResourceId(position, -1));
        textView.setText(nameIDs.getResourceId(position, -1));

        grid.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(FlagsActivity.this, SafeMapActivity.class);
                startActivity(intent);
            }
        });
        textView.setTextAppearance(this, position == 1 ? R.style.TextViewFlags : R.style.TextViewFlagsDisabled);
        imageView.setEnabled(position == 1);
        grid.setEnabled(position == 1);
    }
}
