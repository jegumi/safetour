package com.jegumi.safetour.ui;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jegumi.safetour.R;

public class SafeMapActivity extends ActionBarActivity {

    private GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_map);
        initFields();
        getSupportActionBar().setLogo(R.drawable.ic_home);
    }

    private void initFields() {
        if (map == null) {
            map = ((SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map)).getMap();

            if (map != null) {
                setUpMap();
                addMarkers();
            }
        }
    }

    private void setUpMap() {
        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(
                40.41711, -3.70311));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);

        BitmapDescriptor iconWarning = BitmapDescriptorFactory
                .fromResource(R.drawable.icon_warning);
        BitmapDescriptor iconSafe = BitmapDescriptorFactory
                .fromResource(R.drawable.icon_safe);

        map.moveCamera(center);
        map.animateCamera(zoom);
    }
    
    // This method is a workaround. This should be parsed by a json/xml file or retrieved from a server
    // but right now the information comes in a word file, and I don't have time to convert it
    private void addMarkers() {
        map.addMarker(new MarkerOptions()
                .position(new LatLng(40.416919, -3.703334))
                .title("Puerta del Sol")
                .icon(iconWarning)
                .snippet(
                        "Pickpockets and thieves are common in this area. Hide your most valuable belongings (photo and"
                                + " video camera…) and keep then safely. Watch out the square and the surrounding areas!"));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(40.415457, -3.707422))
                .title("Plaza Mayor")
                .icon(iconWarning)
                .snippet(
                        "Pickpockets and thieves are common in this area. Hide your most valuable belongings "
                                + "(photo and video camera…) and keep them safely. Besides, usually establishments in this area"
                                + " are more expensive due to the large number of tourists."));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(40.418491, -3.713117))
                .title("Palacio Real")
                .icon(iconWarning)
                .snippet(
                        "Do not pay attention to rose sellers and associations that ask you to sign fake and tricky"
                                + " documents in order to get you distracted so they can rob you. Under no circumstances"
                                + " them get close!"));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(40.421272, -3.707589))
                .title("Gran Vía")
                .icon(iconWarning)
                .snippet(
                        "Pickpockets and thieves are common in this area. Hide your most valuable belongings (photo and video camera…) and keep then  safely."));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(40.423706, -3.711761))
                .title("Plaza España")
                .icon(iconWarning)
                .snippet(
                        "During the nights is common to find groups of teenagers drinking and unpleasant situations"
                                + "might occur. You should sightseeing it during the day."));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(40.417051, -3.70331))
                .title("Montera")
                .icon(iconWarning)
                .snippet(
                        "Prostitutes and procurers are common in this street. If you are not interested you better"
                                + "ignore them."));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(40.414744, -3.700749))
                .title("Plaza de Santa Ana")
                .icon(iconWarning)
                .snippet(
                        "Pickpockets and thieves are common in this area. If you are in a terrace’s bar do not leave"
                                + " your belongings on the table. Usually establishments in this area are more expensive due to"
                                + " the large number of tourists. People who offer massages are not professionals. You may get"
                                + " injured."));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(40.410735, -3.7072))
                .title("El Rastro")
                .icon(iconWarning)
                .snippet(
                        "Pickpockets and thieves are common in this area. Hide your most valuable belongings (photo"
                                + "and video camera…) and keep then safely. Be careful while you are buying or go window"
                                + " shopping."));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(40.413782, -3.692202))
                .title("Museo del Prado")
                .icon(iconWarning)
                .snippet(
                        "Pickpockets and thieves are common in this area. Hide your most valuable belongings (photo"
                                + "and video camera…) and keep then safely."));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(40.419598, -3.688168))
                .title("Retiro")
                .icon(iconWarning)
                .snippet(
                        "Pickpockets and thieves are common in this area. Hide your most valuable belongings (photo"
                                + "and video camera…) and keep then safely. People who offer massages are not professionals."
                                + " You may get injured."));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(40.421628, -3.710169))
                .title("Foreign tourist police station")
                .icon(iconSafe)
                .snippet(
                        "If you are a crime victim or on an emergency situation, police will help you."
                                + "\nPhone number: +34 913221027 ; Emergency Phone number: 112"));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(40.412985, -3.694996))
                .title("Police Station Madrid Retiro")
                .icon(iconSafe)
                .snippet(
                        "If you are a crime victim or on an emergency situation, police will help you."
                                + "\nPhone number: +34 917110354 ; Emergency Phone number 112"));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(40.417862, -3.702632))
                .title("Municipal Citizen Service")
                .icon(iconSafe)
                .snippet(
                        "If you have any NOT URGENT problem, any necessity or any complaint, Municipal Police can"
                                + " solve them.\nPhone number: +34 915 234 594 ; Emergency Phone number: 112"
                                + "\nSchedule: From Monday to Friday, From 09:00 to 14:00 and 16:00 to 21:00."));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(40.421562, -3.705499))
                .title("Servicio de Atención al Ciudadano")
                .icon(iconSafe)
                .snippet(
                        "If you have any NOT URGENT problem, any necessity or any complaint, Municipal Police can"
                                + "solve them. \nPhone number: + 34 915 329 412 ; Emergency Phone number 112"
                                + "\nSchedule: From Monday to Friday, From 09:00 to 14:00 and 16:00 to 21:00."));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(40.431432, -3.706836))
                .title("Hospital Universitario Madrid")
                .icon(iconSafe)
                .snippet(
                        "The Hospital has all medical and surgery specialties for adults, besides emergency services"
                                + " available 24 hours. If you have European Sanitary Card the attention will be free, if not"
                                + "you must consult your travel insurance. \nPhone number: +34 91 447 66 00; Emergency"
                                + "Phone number: 112"));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(40.37519, -3.695861))
                .title("Hospital Universitario 12 de Octubre")
                .icon(iconSafe)
                .snippet(
                        "Nowadays, is one of the most prestigious hospitals. It has all medical and surgery"
                                + " specialties for adults, besides emergency services available 24 hours. If you have European"
                                + " Sanitary Card the attention will be free, if not you must consult your travel insurance. "
                                + "\nPhone number: +34 91-390-80-00; Emergency Phone number: 112"));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(40.412548, -3.699459))
                .title("Chemistry/Drugstore 24h")
                .icon(iconSafe)
                .snippet(
                        "This chemistry/drugstore is open 24 hours.\nPhone number: +34 913 692 000"));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(40.415569, -3.709742))
                .title("Chemistry/Drugstore 24h")
                .icon(iconSafe)
                .snippet(
                        "This chemistry/drugstore is open 24 hours.\nPhone number: +34 915 480 014"));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(40.415847, -3.707414))
                .title("Tourist Information Centre")
                .icon(iconSafe)
                .snippet(
                        "It offers all the tourist and cultural information about Madrid, city and metro maps, tourist"
                                + " advice and also official guided visits in many languages. It has free Wi-Fi and free audio"
                                + " guide and virtual maps downloads.\nSchedule: From 09:30 to 20:30, every day"));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(40.420102, -3.705829))
                .title("Tourist Information Office")
                .icon(iconSafe)
                .snippet(
                        "It offers all the tourist and cultural information about Madrid, city and metro maps and also"
                                + " tourist advice.\nSchedule: From 09:30 to 20:30, every day"));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(40.435152, -3.686528))
                .title("USA Embassy")
                .icon(iconSafe)
                .snippet(
                        "Phone number : +34 91 587 22 00\nWebsite: http://spanish.madrid.usembassy.gov/"));

        map.setInfoWindowAdapter(new InfoWindowAdapter() {

            // Use default InfoWindow frame
            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker arg0) {

                View v = getLayoutInflater().inflate(
                        R.layout.info_window_layout, null);

                TextView tvLat = (TextView) v.findViewById(R.id.tv_title);
                TextView tvLng = (TextView) v.findViewById(R.id.tv_description);
                tvLat.setText(arg0.getTitle());
                tvLng.setText(arg0.getSnippet());

                return v;
            }
        });

    }
}
