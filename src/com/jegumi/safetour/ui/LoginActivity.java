package com.jegumi.safetour.ui;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.jegumi.safetour.R;

public class LoginActivity extends RoboActivity {

    @InjectView(R.id.login_button)
    private Button loginButton;
    @InjectView(R.id.login_facebook_button)
    private Button loginFbButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        initFields();
    }

    private void initFields() {
        loginButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                initFlags();
            }
        });
        loginFbButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                initFlags();
            }
        });
    }

    private void initFlags() {
        Intent intent = new Intent(this, FlagsActivity.class);
        startActivity(intent);
    }
}
